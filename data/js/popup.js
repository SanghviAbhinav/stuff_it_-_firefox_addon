var cogshow = 0, allvershowing = 0, versionchange_condition = 0;
var versionchange_array = new Array();
var arr = new Array();

window.addEventListener("message" , receiveMessageOnce, false);
function receiveMessageOnce(event){
	if(event.data == "getData"){
		LiveStuffPlugin.initialize("","","","");
	}
	
	if(event.data=='loggedin'){
		var cog = document.getElementsByClassName("cogimage")[0];
		cog.style.display = "block";
	}
	
	if(event.data[0]=='allversions'){
		var vercont = document.getElementsByClassName("other_versions")[0];
		vercont.innerHTML = "";
		
		for(var m=1; m<event.data.length; m++){
			var thisname = event.data[m]["Id"];
			var thisval = event.data[m]["Value"];
			
			var mainstr = document.createElement("div");
			mainstr.className = "select_version";
			
			if(thisname == "System Version__Dev"){
				mainstr.textContent = "Dev : "+thisval;
				mainstr.setAttribute("ver-type","dev");
			}
			
			mainstr.setAttribute("onclick", "versionChange(this)");
			mainstr.setAttribute("ver-val",thisval);
			vercont.appendChild(mainstr);
		}
		
		for(var m=1; m<event.data.length; m++){
			var thisname = event.data[m]["Id"];
			var thisval = event.data[m]["Value"];
			
			var mainstr = document.createElement("div");
			mainstr.className = "select_version";
			
			if(thisname == "System Version__Staging"){
				mainstr.textContent = "Stage : "+thisval;
				mainstr.setAttribute("ver-type","stage");
			}
			
			mainstr.setAttribute("onclick", "versionChange(this)");
			mainstr.setAttribute("ver-val",thisval);
			vercont.appendChild(mainstr);
		}
		
		for(var m=1; m<event.data.length; m++){
			var thisname = event.data[m]["Id"];
			var thisval = event.data[m]["Value"];
			
			var mainstr = document.createElement("div");
			mainstr.className = "select_version";
		
			if(thisname == "System Version"){
				mainstr.textContent = "Live : "+thisval;
				mainstr.setAttribute("ver-type","live");
			}
			mainstr.setAttribute("onclick", "versionChange(this)");
			mainstr.setAttribute("ver-val",thisval);
			vercont.appendChild(mainstr);
			
		}
	}
	
	if(event.data[0] == 'versionchange'){
		versionchange_condition = 1;
		versionchange_array = event.data;
		LiveStuffPlugin.initialize("","","","");
	}
}

var LiveStuffPlugin = {
	initialize: function (newUrl, newTitle, version, storageversion){
		if(newUrl=="" ||newUrl==null || newTitle==''||newTitle==null){
		}else{
			arr = [];
			arr.push("addonData");
			arr.push(newTitle);
			arr.push(newUrl);
		}
		
		var receiver = document.getElementById('Receiver').contentWindow;
		receiver.postMessage(arr, "*");
		/* Initialize plugin*/
		this._initializeEvents();
		
		if(version==""){
		}else{
			var versiondiv = document.getElementById("version");
			versiondiv.textContent = version;
		}
		
		if(storageversion.length>0){
			var thistype = storageversion[0];
			var thisver = storageversion[1];
			var url = "";
			var iframe = document.getElementById("Receiver");
			if(thistype == 'live'){
				url = "http://livestuff.com";
			}
			else if(thistype == 'dev'){
				url = "http://dev.livestuff.com";
			}
			else if(thistype == 'stage'){
				url = "http://stage.livestuff.com";
			}
			iframe.setAttribute("src", url+"/BrowserAddon?PreviewMode=Y&aRebuild=Y");

			var pointver = thisver.substring(0,1)+"."+thisver.substring(4,5)+"."+thisver.substring(5,6);
			var vercontainer = document.getElementById("version");
			vercontainer.textContent = pointver;
			
			var cog = document.getElementsByClassName("cogimage")[0];
			cog.style.display = "none";
			cogshow = 0;
			var cogopt = document.getElementsByClassName("cogoptions")[0];
			cogopt.style.display = "none";
		}
		
		if( versionchange_condition == "1"){
			versionchange_condition = 0;
			self.port.emit("versionchange", versionchange_array);
			versionchange_array = [];
		}
	},
	_initializeEvents: function (){
		var cogimg = document.getElementsByClassName("cogimage")[0];
		cogimg.onclick=function(){
			var cogopt=document.getElementsByClassName("cogoptions")[0];
			if(cogshow == 0){
				cogshow =1;
				cogopt.style.display = "block";
			}
			else if(cogshow == 1){
				cogshow = 0;
				cogopt.style.display = "none";
			}
		}
		
		var closeicon = document.getElementById("closepopup");
		closeicon.onclick = function(){
			self.port.emit("close-clicked");
		}
		
		var logout = document.getElementById("LogoutButton");
		logout.onclick=function(){
			var cog = document.getElementsByClassName("cogimage")[0];
			cog.style.display = "none";
			cogshow = 0;
			var cogopt = document.getElementsByClassName("cogoptions")[0];
			cogopt.style.display = "none";
			
			var receiver = document.getElementById('Receiver').contentWindow;
			receiver.postMessage("logout", "*");
		}
		
		var verpar = document.getElementsByClassName("current_version")[0];
		verpar.onclick = function(){
			if(allvershowing==0){
				var vercont = document.getElementsByClassName("other_versions")[0];
				vercont.style.display = "block";
				allvershowing = 1;
			}
			else if(allvershowing==1){
				var vercont = document.getElementsByClassName("other_versions")[0];
				vercont.style.display = "none";
				allvershowing = 0;
			}
		}
	}
}

function versionChange(verclicked){
	var thisver = verclicked.getAttribute("ver-val");
	var thistype = verclicked.getAttribute("ver-type");

	var url = "";
	var iframe = document.getElementById("Receiver");
	if(thistype == 'live'){
		url = "http://livestuff.com";
	}
	else if(thistype == 'dev'){
		url = "http://dev.livestuff.com";
	}
	else if(thistype == 'stage'){
		url = "http://stage.livestuff.com";
	}
	iframe.setAttribute("src", url+"/BrowserAddon?PreviewMode=Y&aRebuild=Y");
	
	var pointver = thisver.substring(0,1)+"."+thisver.substring(4,5)+"."+thisver.substring(5,6);
	var vercontainer = document.getElementById("version");
	vercontainer.textContent = pointver;
	
	var vercont = document.getElementsByClassName("other_versions")[0];
	vercont.style.display = "none";
	allvershowing = 0;
				
	var cogopt=document.getElementsByClassName("cogoptions")[0];
	cogopt.style.display = "none";
	cogshow = 0;
	
	var temparr = new Array();
	temparr.push("versionchange", thistype, thisver);
	window.postMessage(temparr, "*")
}

LiveStuffPlugin.self = self;
if (self.port !== undefined){
	self.port.on("show", function(currentLink, currentTitle, currentVersion, availableversion) {
		LiveStuffPlugin.initialize(currentLink, currentTitle, currentVersion, availableversion);
	});
}