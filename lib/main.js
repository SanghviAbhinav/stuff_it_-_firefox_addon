const widgets = require('sdk/widget');
const data = require('sdk/self').data;
const panel = require('sdk/panel');
const passwords = require("sdk/passwords");
const tabs = require("sdk/tabs");
var { ToggleButton } = require('sdk/ui/button/toggle');
var cm = require("sdk/context-menu");
var self = require("sdk/self");
var ss = require("sdk/simple-storage");
if (!ss.storage.versionDetail){
	ss.storage.versionDetail = [];
}

var currentUrl, currentTitle, vertemp;

var main_panel = panel.Panel({
    contentURL: data.url("popup.html"),
    contentScriptFile: [
        data.url('js/jquery-1.10.2.min.js'),
        data.url("js/popup.js")
    ],
    width: 350,
    height: 400
});

var button = ToggleButton({
	id: "stuff_it_beta",
	label: "Stuff It",
	icon: {
		"16": "./img/icon_main_16.png",
		"32": "./img/icon_main_32.png",
		"64": "./img/icon_main_64.png"
    },
	onChange : handleChange
});

function handleChange(state){
	currentUrl = tabs.activeTab.url;
	currentTitle = tabs.activeTab.title;
	vertemp = self.version;
	if(state.checked){
		main_panel.show({
			position : button
		});

		button.state("window", {
			checked: false
		});
	}
}

cm.Item({
  label: "Stuff It",
  image : data.url("./img/icon_16.png"),
  contentScript : 'self.on("click", function(node, data){'+
	'self.postMessage(node.src);' +
  '});',
  onMessage: function () {
	currentUrl = tabs.activeTab.url;
	currentTitle = tabs.activeTab.title;
	vertemp = self.version;
    main_panel.show({});
  }
});

main_panel.on("show", function() {
    main_panel.port.emit("show", currentUrl, currentTitle, vertemp, ss.storage.versionDetail);
});

main_panel.port.on("close-clicked", function(){
	main_panel.hide();
});

main_panel.port.on("versionchange", function(a){
	ss.storage.versionDetail = [];
	ss.storage.versionDetail.push(a[1], a[2]);
});
